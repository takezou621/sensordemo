//
//  SDViewController.m
//  SensorDemo
//
//  Created by Takeshi Kawai on 12/06/09.
//  Copyright (c) 2012年 None. All rights reserved.
//

#import "SDViewController.h"

@interface SDViewController ()

@end

@implementation SDViewController
@synthesize graphView;
@synthesize proximityLabel;

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  
  // 近接センサ監視
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(proximitySensorStateDidChange:)
                                               name:UIDeviceProximityStateDidChangeNotification
                                             object:nil];
  
}

- (void)viewDidUnload
{
  [self setGraphView:nil];
  [self setProximityLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)proximitySensorStateDidChange:(NSNotification *)notification {
   if ([UIDevice currentDevice].proximityState == NO) {
     self.proximityLabel.text = @"OFF";
   } else {
     self.proximityLabel.text = @"ON";
   }
}

@end
