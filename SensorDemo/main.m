//
//  main.m
//  SensorDemo
//
//  Created by Takeshi Kawai on 12/06/09.
//  Copyright (c) 2012年 None. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDAppDelegate class]));
  }
}
