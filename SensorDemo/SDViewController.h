//
//  SDViewController.h
//  SensorDemo
//
//  Created by Takeshi Kawai on 12/06/09.
//  Copyright (c) 2012年 None. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDGraphView.h"

@interface SDViewController : UIViewController
@property (weak, nonatomic) IBOutlet SDGraphView *graphView;
@property (weak, nonatomic) IBOutlet UILabel *proximityLabel;

@end
