//
//  SDGraphView.h
//  SensorDemo
//
//  Created by Takeshi Kawai on 12/06/09.
//  Copyright (c) 2012年 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDGraphView : UIView<UIAccelerometerDelegate>
{
  NSUInteger nextIndex;
  double history[150][3];
  UILabel* xAxisLabel;
  UILabel* yAxisLabel;
  UILabel* zAxisLabel;
}

@property double xAxis;
@property double yAxis;
@property double zAxis;


- (void)updateHistoryWithX:(double)x y:(double)y z:(double)z;

@end
