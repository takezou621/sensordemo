//
//  SDGraphView.m
//  SensorDemo
//
//  Created by Takeshi Kawai on 12/06/09.
//  Copyright (c) 2012年 None. All rights reserved.
//

#import "SDGraphView.h"

#define kAccelerometerFrequency		100.0 // Hz
#define kFilteringFactor			0.1

@implementation SDGraphView
@synthesize xAxis,yAxis,zAxis;

- (void)updateHistoryWithX:(double)x y:(double)y z:(double)z
{
  history[nextIndex][0] = x * 100;
  history[nextIndex][1] = y * 100;
  history[nextIndex][2] = z * 100;
  
  nextIndex = (nextIndex + 1) % 150;
  
  [self setNeedsDisplay];
}

/**
 罫線
 */
- (void)drawGraphInContext:(CGContextRef)context withBounds:(CGRect)bounds
{
  CGFloat value, temp;
  
  // 前回のグラフ
  CGContextSaveGState(context);
  CGContextSetLineWidth(context, 1.0f);
  
  
  // 中間線を描画
  CGContextSetGrayStrokeColor(context, 0.6, 1.0);
  CGContextBeginPath(context);
  for (value = -5 + 1.0; value <= 5 - 1.0; value += 1.0) {
    if (value == 0.0) {
      continue;
    }
    
    temp = 0.5f + roundf(bounds.origin.y + bounds.size.height / 2 + value / (2 * 5) * bounds.size.height);
    CGContextMoveToPoint(context, bounds.origin.x, temp);
    CGContextAddLineToPoint(context, bounds.origin.x + bounds.size.width, temp);
  }
  CGContextStrokePath(context);
  
  // 中央の罫線を表示
  CGContextSetGrayStrokeColor(context, 0.25, 1.0);
  CGContextBeginPath(context);
  temp = 0.5 + roundf(bounds.origin.y + bounds.size.height / 2);
  CGContextMoveToPoint(context, bounds.origin.x, temp);
  CGContextAddLineToPoint(context, bounds.origin.x + bounds.size.width, temp);
  CGContextStrokePath(context);
  
  // Restore previous graphics state.
  CGContextRestoreGState(context);
}

- (void)drawHistory:(NSUInteger)axis fromIndex:(NSUInteger)index inContext:(CGContextRef)context bounds:(CGRect)bounds {
  CGFloat value;
  
  
  CGContextBeginPath(context);
  for (NSUInteger counter = 0; counter < 150; ++counter) {
    // UIViewでは上下逆さまになるので、反転して描画する
    value = history[(index + counter) % 150][axis] / - 128;
    if (counter > 0) {
      // ０以上の場合のグラフ描画[3]
      CGContextAddLineToPoint(context,
                              bounds.origin.x + (float)counter / (float)(150-1) * bounds.size.width,
                              bounds.origin.y + bounds.size.height / 2 + value * bounds.size.height / 2 );
    }else {
      // for文の最初の一回だけここを通る
      // 0以下の場合のグラフ描画[2]
      CGContextMoveToPoint(context, 
                           bounds.origin.x + (float)counter/ (float)(150 -1) * bounds.size.width, 
                           bounds.origin.y + bounds.size.height / 2 + value * bounds.size.height / 2);
      
    }
    

    
  }
  

  
  CGContextSaveGState(context);
  CGContextSetRGBStrokeColor(context,
                             (axis == 0 ? 1.0 : 0.0),
                             (axis == 1 ? 0.5 : 0.0),
                             (axis == 2 ? 1.0 : 0.0), 1.0);
  CGContextSetLineWidth(context, 1.0);
  CGContextStrokePath(context); // [4]
  
  NSString* string = [NSString stringWithFormat:@"%f",value];
  switch (axis) {
    case 0:
      [string drawAtPoint:CGPointMake(10, 10)
                 withFont:[UIFont systemFontOfSize:12]];
      break;
    case 1:
      [string drawAtPoint:CGPointMake(10, 30)
                 withFont:[UIFont systemFontOfSize:12]];
      break;
    case 2:
      [string drawAtPoint:CGPointMake(10, 50)
                 withFont:[UIFont systemFontOfSize:12]];
      break;      
    default:
      break;
  }
  
  CGContextRestoreGState(context);

}

/**
 描画を行う際に呼ばれるメソッド
 */
-(void)drawRect:(CGRect)clip
{
  NSInteger index = nextIndex;
  
  // 加速度センサーの感知感覚を1秒に設定
  [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0/kAccelerometerFrequency];
  [[UIAccelerometer sharedAccelerometer] setDelegate:self];
  
  // コンテキスト取得
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // 描画領域(自身のサイズ)
  CGRect bounds = CGRectMake(0, 0, [self bounds].size.width, [self bounds].size.height);
  
  // 罫線などを描画
  [self drawGraphInContext:context withBounds:bounds];
  
  // アンチエイリアスをOFF
  CGContextSetAllowsAntialiasing(context, FALSE);
  
  // XYZ軸を描画
  for (NSUInteger i = 0; i < 3; i++) {
    [self drawHistory:i fromIndex:index inContext:context bounds:bounds];
  }
  
  // アンチエイリアスをONに戻す
  CGContextSetAllowsAntialiasing(context, TRUE);
}

#pragma mark - UIAccelerometerDelegate
-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration
{
  [self updateHistoryWithX:acceleration.x y:acceleration.y z:acceleration.z];


  
  yAxis = acceleration.y;
  zAxis = acceleration.z;
}
@end
